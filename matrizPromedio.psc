subProceso str <- formatear(string,esp,relleno)
	Definir str como Cadena;
	Definir l,e,i,ord como Entero;
	l <- LONGITUD(string);
	e <- CONVERTIRANUMERO(esp);
	str <- string;
	Si l != e Entonces
		Si l>e Entonces
			str <- SUBCADENA(str,0,e-2);
			str <- CONCATENAR(str,"*");
		SiNo
			i <- l;
			Mientras i != e Hacer
				str <- CONCATENAR(relleno,str);
				i <- i + 1;
			FinMientras
		FinSi
	FinSi
FinSubProceso
Proceso matrizPromedio
	Definir mat,ac,aux como real;
	Definir i,j,c,ord,k como entero;
	Dimension mat[5,5],aux[5];
	ac <- 0;
	ord <- 0;
	c <- 0;
	//Carga
	Escribir "Carga la matriz A";
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		Para j<-0 Hasta 3 Con Paso 1 Hacer
			Escribir "Ingrese A",i+1,j+1,": ";
			Leer mat[i,j];
			ac <- ac + mat[i,j];
		FinPara
		mat[i,4] <- ac/4;
		ac <- 0;
	FinPara
	//Matriz Mostrar
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		Para j<-0 Hasta 4 Con Paso 1 Hacer
			Escribir sin saltar "  ",formatear(convertiratexto(mat[i,j]),"6"," ");
		FinPara
		Escribir "";
	FinPara
	// Ordenar por promedio
	Mientras ord != 1 Hacer
		Para i<-1 Hasta 4 Con Paso 1 Hacer
			si mat[i,4] > mat[i-1,4] entonces //Mayor a menor
			//si mat[i,4] < mat[i-1,4] entonces //Menor a mayor
				//cargar auxiliar
				Para k<-0 Hasta 4 Con Paso 1 Hacer
					aux[k] <- mat[i,k];
				FinPara
				//Reasignar
				Para k<-0 Hasta 4 Con Paso 1 Hacer
					mat[i,k] <- mat[i-1,k];
				FinPara
				Para k<-0 Hasta 4 Con Paso 1 Hacer
					mat[i-1,k] <- aux[k];
				FinPara
			SiNo
				c <- c + 1;
			FinSi
		FinPara
		si c = 4 Entonces
			ord <- 1;
		FinSi
		c<-0;
	FinMientras
	Escribir "Matriz ordenada por promedio de filas: ";
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		Para j<-0 Hasta 4 Con Paso 1 Hacer
			Escribir sin saltar "  ",formatear(convertiratexto(mat[i,j]),"6"," ");
		FinPara
		Escribir "";
	FinPara
FinProceso
